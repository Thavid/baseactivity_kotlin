package com.thavid.baseactivitykotlin.screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.thavid.baseactivitykotlin.R
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_base)

        tool_bar.setOnClickListener {
            toolbarEvent()
        }
    }

    override fun setContentView(layoutResID: Int) {
        val view= layoutInflater.inflate(layoutResID,null,false)
        content_view.addView(view)
    }

    protected open fun toolbarEvent(){

    }
}
