package com.thavid.baseactivitykotlin.screen

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.thavid.baseactivitykotlin.R

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun toolbarEvent() {
        super.toolbarEvent()
        Toast.makeText(this, "Click hz b", Toast.LENGTH_SHORT).show()
    }
}
